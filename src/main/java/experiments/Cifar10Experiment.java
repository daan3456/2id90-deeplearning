package experiments;

import java.io.IOException;
import java.util.List;
import layer.BatchNorm;
import layer.Dropout;
import nl.tue.s2id90.dl.NN.Model;
import nl.tue.s2id90.dl.NN.activation.Linear;
import nl.tue.s2id90.dl.NN.activation.RELU;
import nl.tue.s2id90.dl.NN.initializer.Gaussian;
import nl.tue.s2id90.dl.NN.layer.Convolution2D;
import nl.tue.s2id90.dl.NN.layer.Flatten;
import nl.tue.s2id90.dl.NN.layer.FullyConnected;
import nl.tue.s2id90.dl.NN.layer.InputLayer;
import nl.tue.s2id90.dl.NN.layer.OutputSoftmax;
import nl.tue.s2id90.dl.NN.layer.PoolMax2D;
import nl.tue.s2id90.dl.NN.loss.CrossEntropy;
import nl.tue.s2id90.dl.NN.optimizer.Optimizer;
import nl.tue.s2id90.dl.NN.optimizer.SGD;
import nl.tue.s2id90.dl.NN.optimizer.update.GradientDescent;
import nl.tue.s2id90.dl.NN.tensor.TensorPair;
import nl.tue.s2id90.dl.NN.tensor.TensorShape;
import nl.tue.s2id90.dl.NN.transform.DataTransform;
import nl.tue.s2id90.dl.NN.validate.Classification;
import nl.tue.s2id90.dl.experiment.Experiment;
import nl.tue.s2id90.dl.input.Cifar10Reader;
import nl.tue.s2id90.dl.input.InputReader;
import nl.tue.s2id90.dl.javafx.FXGUI;
import nl.tue.s2id90.dl.javafx.ShowCase;
import transform.MeanSubtraction;
import transform.Normalizer;
import update.AdaDelta;
import update.GradientDescentMomentum;
import update.L2Decay;

/**
 *
 * @author s167501
 */
public class Cifar10Experiment extends Experiment {
    int batchSize = 128; //number of training samples per batch
    int epochs = 12; //number of iterations of the training process
    float learningRate = 1f; //rate of optimization of the network weights
    double momentumBeta = 0.9;
    double weightDecay = 0.0001;
    double rho = 0.9;
    double eps = 0.000001;
    
    String[] labels = (String[]) Cifar10Reader.getLabelsAsString().toArray();
    
    Cifar10Experiment() { super(true); }
    
    public void go() throws IOException {
        Cifar10Reader reader = new Cifar10Reader(batchSize, 10);
        
        // Transform the data
        transformData(reader.getTrainingData(), reader.getValidationData());
        
        int inputs = reader.getInputShape().getNeuronCount();
        int outputs = reader.getOutputShape().getNeuronCount();
        System.out.println("Inputs: " + inputs + " - Outputs: " + outputs);
        
        System.out.println("Reader info:\n" + reader.toString());
        reader.getValidationData(1).forEach(System.out::println);
        ShowCase showCase = new ShowCase(i -> labels[i]);
        FXGUI.getSingleton().addTab("show case", showCase.getNode());
        showCase.setItems(reader.getValidationData(100));
        
        Model model = createModel(inputs, outputs);
        
        Optimizer sgd = SGD.builder()
                .model(model)
                .validator(new Classification())
                .learningRate(learningRate)
                .updateFunction(() -> new L2Decay(() -> new AdaDelta(rho, eps), weightDecay))
                .build();
        
        trainModel(model, reader, sgd, epochs, 0);
    }
    
    void transformData(List<TensorPair> trainingData, List<TensorPair> validationData) {
        DataTransform dt = new Normalizer();
        dt.transform(trainingData);
        dt.transform(validationData);
    }
    
    Model createModel(int inputs, int outputs) {
        final int res = 32;
        
        int conv1Depth = 32;
        int conv2Depth = 32;
        
        Model model = new Model(new InputLayer("In", new TensorShape(res, res, 3), true));
        
        model.addLayer(new BatchNorm("batchNorm", new TensorShape(32, 32, 3), 1, 0, 0.9, eps));
        
        model.addLayer(new Convolution2D("Conv2D", new TensorShape(res, res, 3), 5, conv1Depth, new RELU()));
        model.addLayer(new Convolution2D("Conv2D2", new TensorShape(res, res, conv1Depth), 3, conv1Depth, new RELU()));
        model.addLayer(new PoolMax2D("Pool2D", new TensorShape(res, res, conv1Depth), 2));
        
        model.addLayer(new Dropout("Dropout1", new TensorShape(16, 16, conv1Depth), 0.25f));
        
        model.addLayer(new Convolution2D("Conv23", new TensorShape(res/2, res/2, conv1Depth), 3, conv2Depth, new RELU()));
        model.addLayer(new PoolMax2D("Pool2D2", new TensorShape(res/2, res/2, conv2Depth), 2));
        
        model.addLayer(new Dropout("Dropout2", new TensorShape(8, 8, conv2Depth), 0.25f));
        
        model.addLayer(new Flatten("Flatten", new TensorShape(res/4, res/4, conv2Depth)));
        
        model.addLayer(new FullyConnected("fcl", new TensorShape(res/4*res/4*conv2Depth), 256, new RELU()));
        
        model.addLayer(new Dropout("Dropout3", new TensorShape(256), 0.5f));
        
        model.addLayer(new FullyConnected("fcl2", new TensorShape(256), 10, new RELU()));
        model.addLayer(new OutputSoftmax("Out", new TensorShape(10), outputs, new CrossEntropy()));

        model.initialize(new Gaussian());
        System.out.println("---");
        System.out.println(model);
        System.out.println("---");
        return model;
    }

    public static void main(String[] args) throws IOException {
        new Cifar10Experiment().go();
    }
}
