package experiments;

import nl.tue.s2id90.dl.experiment.Experiment;
import java.io.IOException;
import nl.tue.s2id90.dl.NN.Model;
import nl.tue.s2id90.dl.NN.activation.RELU;
import nl.tue.s2id90.dl.NN.initializer.Gaussian;
import nl.tue.s2id90.dl.NN.layer.FullyConnected;
import nl.tue.s2id90.dl.NN.layer.InputLayer;
import nl.tue.s2id90.dl.NN.layer.SimpleOutput;
import nl.tue.s2id90.dl.NN.loss.MSE;
import nl.tue.s2id90.dl.NN.optimizer.Optimizer;
import nl.tue.s2id90.dl.NN.optimizer.SGD;
import nl.tue.s2id90.dl.NN.tensor.TensorShape;
import nl.tue.s2id90.dl.NN.validate.Regression;
import nl.tue.s2id90.dl.experiment.BatchResult;
import nl.tue.s2id90.dl.input.GenerateFunctionData;
import nl.tue.s2id90.dl.input.InputReader;

/**
 *
 * @author s166499 and s167501
 */
public class FunctionExperiment extends Experiment {
    static int batchSize = 20; //number of training samples per batch
    static int epochs = 20; //number of iterations of the training process
    static float learningRate = 0.1f; //rate of optimization of the network weights
    
    FunctionExperiment() { super(true); }
    
    public float go(int batchSize, int epochs, float learningRate) throws IOException {
        InputReader reader = GenerateFunctionData.THREE_VALUED_FUNCTION(batchSize);
        int inputs = reader.getInputShape().getNeuronCount();
        int outputs = reader.getOutputShape().getNeuronCount();
        
        System.out.println("Inputs: " + inputs + " - Outputs: " + outputs);
        System.out.println("Reader info:\n" + reader.toString());
        
        Model model = createModel(inputs, outputs);
        Optimizer sgd = SGD.builder()
                .model(model)
                .validator(new Regression())
                .learningRate(learningRate)
                .build();
        trainModel(model, reader, sgd, epochs, 0);
        return getLastValidationResult().validation;
    }
    
    Model createModel(int inputs, int outputs) {
        int m = inputs;
        int n = outputs * 2;
        Model model = new Model(new InputLayer("In", new TensorShape(inputs), true));
        model.addLayer(new FullyConnected("fc1", new TensorShape(m), n, new RELU()));
        model.addLayer(new SimpleOutput("Out", new TensorShape(n), outputs, new MSE(), true));
        model.initialize(new Gaussian());
        System.out.println(model);
        return model;
    }
    
    public static void main(String[] args) throws IOException {
        new FunctionExperiment().go(batchSize, epochs, learningRate);
    }
}
