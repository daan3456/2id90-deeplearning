package experiments;

import nl.tue.s2id90.dl.experiment.Experiment;
import java.io.IOException;
import java.util.List;
import nl.tue.s2id90.dl.NN.Model;
import nl.tue.s2id90.dl.NN.activation.Linear;
import nl.tue.s2id90.dl.NN.activation.RELU;
import nl.tue.s2id90.dl.NN.initializer.Gaussian;
import nl.tue.s2id90.dl.NN.layer.Convolution2D;
import nl.tue.s2id90.dl.NN.layer.Flatten;
import nl.tue.s2id90.dl.NN.layer.InputLayer;
import nl.tue.s2id90.dl.NN.layer.OutputSoftmax;
import nl.tue.s2id90.dl.NN.layer.PoolMax2D;
import nl.tue.s2id90.dl.NN.loss.CrossEntropy;
import nl.tue.s2id90.dl.NN.optimizer.Optimizer;
import nl.tue.s2id90.dl.NN.optimizer.SGD;
import nl.tue.s2id90.dl.NN.tensor.TensorPair;
import nl.tue.s2id90.dl.NN.tensor.TensorShape;
import nl.tue.s2id90.dl.NN.transform.DataTransform;
import nl.tue.s2id90.dl.NN.validate.Classification;
import nl.tue.s2id90.dl.input.InputReader;
import nl.tue.s2id90.dl.input.PrimitivesDataGenerator;
import nl.tue.s2id90.dl.javafx.FXGUI;
import nl.tue.s2id90.dl.javafx.ShowCase;
import transform.MeanSubtraction;
import update.AdaDelta;
import update.GradientDescentMomentum;
import update.L2Decay;

/**
 *
 * @author s166499 and s167501
 */
public class PrimitivesExperiment extends Experiment {
    static int batchSize = 32; //number of training samples per batch
    static int epochs = 10; //number of iterations of the training process
    static float learningRate = 0.2f; //rate of optimization of the network weights
    double momentumBeta = 0.9f;
    double weightDecay = 0.0001f;
    double rho = 0.95f;
    double eps = 0.000001f;
    
    String[] labels = {"Square", "Circle", "Triangle"};
    PrimitivesExperiment() { super(true); }
    
    public float go(int batchSize, int epochs, float learningRate) throws IOException {
        int seed = 11081961;
        int trainingDataSize = 2500;
        int testDataSize = 600;
        InputReader reader = new PrimitivesDataGenerator(batchSize, seed,
                trainingDataSize, testDataSize);
        // Transform the data
        transformData(reader.getTrainingData(), reader.getValidationData());
        int inputs = reader.getInputShape().getNeuronCount();
        int outputs = reader.getOutputShape().getNeuronCount();
        
        System.out.println("Inputs: " + inputs + " - Outputs: " + outputs);
        System.out.println("Reader info:\n" + reader.toString());
        reader.getValidationData(1).forEach(System.out::println);
        ShowCase showCase = new ShowCase(i -> labels[i]);
        FXGUI.getSingleton().addTab("show case", showCase.getNode());
        showCase.setItems(reader.getValidationData(100));
        
        Model model = createModel(inputs, outputs);
        Optimizer sgd = SGD.builder()
                .model(model)
                .validator(new Classification())
                .learningRate(learningRate)
                .updateFunction(() -> new L2Decay(() -> new AdaDelta(rho, eps), weightDecay))
                .build();
        trainModel(model, reader, sgd, epochs, 0);
        return getLastValidationResult().validation;
    }
    
    void transformData(List<TensorPair> trainingData, List<TensorPair> validationData) {
        DataTransform dt = new MeanSubtraction();
        dt.fit(trainingData);
        dt.transform(trainingData);
        dt.transform(validationData);
    }
    
    Model createModel(int inputs, int outputs) {
        int conv1Depth = 64;
        int conv2Depth = 256;
        Model model = new Model(new InputLayer("In", new TensorShape(28, 28, 1), true));
        model.addLayer(new Convolution2D("Conv2D", new TensorShape(28, 28, 1), 5, conv1Depth, new RELU()));
        model.addLayer(new PoolMax2D("Pool2D", new TensorShape(28, 28, conv1Depth), 2));
        model.addLayer(new Convolution2D("Conv2D2", new TensorShape(14, 14, conv1Depth), 5, conv2Depth, new RELU()));
        model.addLayer(new PoolMax2D("Pool2D2", new TensorShape(14, 14, conv2Depth), 2));
        model.addLayer(new Flatten("Flatten", new TensorShape(7, 7, conv2Depth)));
        model.addLayer(new OutputSoftmax("Out", new TensorShape(7*7*conv2Depth), outputs, new CrossEntropy()));
        model.initialize(new Gaussian());
        System.out.println("---");
        System.out.println(model);
        System.out.println("---");
        return model;
    }

    public static void main(String[] args) throws IOException {
        new PrimitivesExperiment().go(batchSize, epochs, learningRate);
    }
    
}
