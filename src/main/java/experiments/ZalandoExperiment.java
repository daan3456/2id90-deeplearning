package experiments;

import nl.tue.s2id90.dl.experiment.Experiment;
import java.io.IOException;
import java.util.List;
import nl.tue.s2id90.dl.NN.Model;
import nl.tue.s2id90.dl.NN.activation.RELU;
import nl.tue.s2id90.dl.NN.initializer.Gaussian;
import nl.tue.s2id90.dl.NN.layer.Flatten;
import nl.tue.s2id90.dl.NN.layer.FullyConnected;
import nl.tue.s2id90.dl.NN.layer.InputLayer;
import nl.tue.s2id90.dl.NN.layer.OutputSoftmax;
import nl.tue.s2id90.dl.NN.loss.CrossEntropy;
import nl.tue.s2id90.dl.NN.optimizer.Optimizer;
import nl.tue.s2id90.dl.NN.optimizer.SGD;
import nl.tue.s2id90.dl.NN.tensor.TensorPair;
import nl.tue.s2id90.dl.NN.tensor.TensorShape;
import nl.tue.s2id90.dl.NN.transform.DataTransform;
import nl.tue.s2id90.dl.NN.validate.Classification;
import nl.tue.s2id90.dl.input.InputReader;
import nl.tue.s2id90.dl.input.MNISTReader;
import nl.tue.s2id90.dl.javafx.FXGUI;
import nl.tue.s2id90.dl.javafx.ShowCase;
import transform.MeanSubtraction;
import update.GradientDescentMomentum;

/**
 *
 * @author s166499 and s167501
 */
public class ZalandoExperiment extends Experiment {
    static int batchSize = 32; //number of training samples per batch
    static int epochs = 5; //number of iterations of the training process
    static float learningRate = 0.02f; //rate of optimization of the network weights
    float momentumBeta = 0.9f;
    
    String[] labels = {"T-shirt/top", "Trouser", "Pullover", "Dress", "Coat",
        "Sandal", "Shirt", "Sneaker", "Bag", "Ankle boot"};
    ZalandoExperiment() { super(true); }
    
    public float go(int batchSize, int epochs, float learningRate) throws IOException {
        InputReader reader = MNISTReader.fashion(batchSize);
        // Transform the data
        transformData(reader.getTrainingData(), reader.getValidationData());
        int inputs = reader.getInputShape().getNeuronCount();
        int outputs = reader.getOutputShape().getNeuronCount();
        
        System.out.println("Inputs: " + inputs + " - Outputs: " + outputs);
        System.out.println("Reader info:\n" + reader.toString());
        reader.getValidationData(1).forEach(System.out::println);
        ShowCase showCase = new ShowCase(i -> labels[i]);
        FXGUI.getSingleton().addTab("show case", showCase.getNode());
        showCase.setItems(reader.getValidationData(100));
        
        Model model = createModel(inputs, outputs);
        Optimizer sgd = SGD.builder()
                .model(model)
                .validator(new Classification())
                .learningRate(learningRate)
                .updateFunction(() -> new GradientDescentMomentum(momentumBeta))
                .build();
        trainModel(model, reader, sgd, epochs, 0);
        return getLastValidationResult().validation;
    }
    
    void transformData(List<TensorPair> trainingData, List<TensorPair> validationData) {
        DataTransform dt = new MeanSubtraction();
        dt.fit(trainingData);
        dt.transform(trainingData);
        dt.transform(validationData);
    }
    
    Model createModel(int inputs, int outputs) {
        int m = 28 * 28;
        int n = outputs;
        Model model = new Model(new InputLayer("In", new TensorShape(28, 28, 1), true));
        model.addLayer(new Flatten("Flatten", new TensorShape(28, 28, 1)));
        model.addLayer(new OutputSoftmax("Out", new TensorShape(m), n, new CrossEntropy()));
        model.initialize(new Gaussian());
        System.out.println(model);
        return model;
    }

    public static void main(String[] args) throws IOException {
        new ZalandoExperiment().go(batchSize, epochs, learningRate);
    }
}
