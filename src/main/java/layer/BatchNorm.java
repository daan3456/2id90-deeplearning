package layer;

import java.util.function.Supplier;
import nl.tue.s2id90.dl.NN.error.IllegalInput;
import nl.tue.s2id90.dl.NN.initializer.Initializer;
import nl.tue.s2id90.dl.NN.layer.Layer;
import nl.tue.s2id90.dl.NN.optimizer.update.UpdateFunction;
import nl.tue.s2id90.dl.NN.tensor.Tensor;
import nl.tue.s2id90.dl.NN.tensor.TensorShape;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

/**
 *
 * @author s167501
 */
public class BatchNorm extends Layer {
    
    private INDArray xHat;
    private INDArray xMu;
    private double iVar;
    private double var;
    
    private double gamma;
    private double beta;
    private double momentum;
    private double eps;
    
    private double dGamma;
    private double dBeta;
    
    
    private Double movingMean = null;
    private Double movingVar = null;

    public BatchNorm(String name, TensorShape shape_input, double gamma, double beta, double momentum, double eps) {
        super(name, shape_input, shape_input);
        this.gamma = gamma;
        this.beta = beta;
        this.momentum = momentum;
        this.eps = eps;
    }

    @Override
    public boolean showValues() {
        return false;
    }

    @Override
    public void initializeLayer(Initializer initializer) {
    }

    @Override
    public Tensor inference(Tensor input) throws IllegalInput {
        
        boolean training = this.isInTrainingMode();
        
        // Compute the mean of the input
        double mean = (double) input.getValues().meanNumber();
        
        // Subtract the mean from the inputs
        xMu = input.getValues().sub(mean);

        // Compute the variance of the input
        var = (double) xMu.mul(xMu).meanNumber();
        
        if (training) {
            // Compute xHat with the mean and var over the mini-batch
            iVar = 1/Math.sqrt(var + eps);

            // Compute xHat
            xHat = xMu.mul(iVar);
            
            // Update the moving mean and var that are used in testing
            if (movingMean == null) {
                movingMean = mean;
            } else {
                movingMean = movingMean * momentum + mean * (1 - momentum);
            }

            if (movingVar == null) {
                movingVar = var;
            } else {
                movingVar = movingVar * momentum + var * (1 - momentum);
            }
        
        } else {
            // In testing mode, compute xHat with the movingMean and movingVar
            xHat = input.getValues().sub(movingMean).divi(Math.sqrt(movingVar + eps));
            
        }
        
        // Return the scaled and shifted result
        INDArray gammaX = xHat.mul(gamma);
        return new Tensor(gammaX.add(beta), outputShape);
    }

    @Override
    public INDArray backpropagation(INDArray input) {
        // Compute the beta back propagation error
        dBeta = (double) input.sumNumber();
        
        // Compute the gamma back propagation error
        dGamma = (double) input.mul(xHat).sumNumber();
        
        // Compute the backward propagation error for the layer input
        double factor = (1 / input.shape().length) * gamma * iVar;
        INDArray pos = input.mul(input.shape().length).subi(input.sumNumber());
        INDArray neg = xMu.div(var + eps).muli(input.mul(xMu).sumNumber());
        return pos.subi(neg).muli(factor);

    }

    @Override
    public void updateLayer(float learning_rate, int batch_size) {
        float factor = -learning_rate / batch_size;
        gamma += factor * dGamma;
        beta += factor * dBeta;
        dGamma = 0;
        dBeta = 0;
    }
    
    UpdateFunction gammaUpdater, betaUpdater;
    @Override
    public void updateLayer(Supplier<UpdateFunction> createUpdateFunction, float learning_rate, int batch_size) {
        if (gammaUpdater==null) gammaUpdater=createUpdateFunction.get();
        if (betaUpdater==null) betaUpdater=createUpdateFunction.get();
        
        INDArray g = Nd4j.create(new double[] { gamma } );
        INDArray b = Nd4j.create(new double[] { beta } );
        INDArray dg = Nd4j.create(new double[] { dGamma } );
        INDArray db = Nd4j.create(new double[] { dBeta } );
        gammaUpdater.update(g, true, learning_rate, batch_size, dg);      // also fills dGamma with zeroes
        betaUpdater.update(b, true, learning_rate, batch_size, db);   // also fills dBeta with zeroes
        gamma = g.getDouble(0);
        beta = b.getDouble(0);
        dGamma = 0;
        dBeta = 0;
    }
    
}
