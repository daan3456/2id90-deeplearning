/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package layer;

import java.util.logging.Level;
import java.util.logging.Logger;
import nl.tue.s2id90.dl.NN.activation.Activation;
import nl.tue.s2id90.dl.NN.error.IllegalInput;
import nl.tue.s2id90.dl.NN.initializer.Initializer;
import nl.tue.s2id90.dl.NN.layer.FullyConnected;
import nl.tue.s2id90.dl.NN.layer.Layer;
import nl.tue.s2id90.dl.NN.tensor.Tensor;
import nl.tue.s2id90.dl.NN.tensor.TensorShape;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.api.rng.distribution.impl.BinomialDistribution;
import org.nd4j.linalg.factory.Nd4j;

/**
 *
 * @author s167501
 */
public class Dropout extends Layer {
    
    private float p;
    
    private INDArray dropoutArray;

    public Dropout(String name, TensorShape shape_input, float p) {
        super(name, shape_input, shape_input);
        this.p = 1 - p; // Invert, since it should be the dropout percentage
    }

    @Override
    public boolean showValues() {
        return false;
    }

    @Override
    public void initializeLayer(Initializer initializer) {
    }

    @Override
    public Tensor inference(Tensor input) throws IllegalInput {
        boolean training = this.isInTrainingMode();
        if (training) {
            dropoutArray = Nd4j.rand(input.getValues().shape(), new BinomialDistribution(1, p)).divi(p);
            input.getValues().muli(dropoutArray);
        }
        return input;
    }

    @Override
    public INDArray backpropagation(INDArray input) {
        if (dropoutArray == null) {
            throw new IllegalStateException("dropoutArray is null");
        }
        return input.mul(dropoutArray);
    }

    @Override
    public void updateLayer(float learning_rate, int batch_size) {
    }
    
}
