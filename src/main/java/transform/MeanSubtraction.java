/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transform;

import java.util.List;
import nl.tue.s2id90.dl.NN.tensor.Tensor;
import nl.tue.s2id90.dl.NN.tensor.TensorPair;
import nl.tue.s2id90.dl.NN.transform.DataTransform;
import org.nd4j.linalg.api.ndarray.INDArray;

/**
 *
 * @author s167501
 */
public class MeanSubtraction implements DataTransform {
    
    float mean;

    @Override
    public void fit(List<TensorPair> pairs) {
        if (pairs.isEmpty()) {
            throw new IllegalArgumentException("Empty dataset");
        }
        for (TensorPair pair : pairs) {
            Tensor t = pair.model_input;
            INDArray a = t.getValues();
            mean += a.meanNumber().floatValue();
        }
        mean /= pairs.size();
    }

    @Override
    public void transform(List<TensorPair> pairs) {
        for (TensorPair pair : pairs) {
            pair.model_input.getValues().subi(mean);
        }
    }
    
}
