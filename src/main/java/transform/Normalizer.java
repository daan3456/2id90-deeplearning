
package transform;

import java.util.List;
import nl.tue.s2id90.dl.NN.tensor.TensorPair;
import nl.tue.s2id90.dl.NN.transform.DataTransform;

/**
 *
 * @author s167501
 */
public class Normalizer implements DataTransform {

    @Override
    public void fit(List<TensorPair> pairs) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void transform(List<TensorPair> pairs) {
        for (TensorPair pair : pairs) {
            pair.model_input.getValues().subi(128);
            pair.model_input.getValues().divi(128);
        }
    }
    
}
