package update;

import nl.tue.s2id90.dl.NN.optimizer.update.UpdateFunction;
import org.nd4j.linalg.api.ndarray.INDArray;
import static org.nd4j.linalg.ops.transforms.Transforms.sqrt;

/**
 *
 * @author s167501
 */
public class AdaDelta implements UpdateFunction {
    
    private double rho;
    private double eps;
    
    private INDArray storedGradient;
    private INDArray storedDeltas;
    
    public AdaDelta(double rho, double eps) {
        this.rho = rho;
        this.eps = eps;
    }

    @Override
    public void update(INDArray array, boolean isBias, float learningRate, int batchSize, INDArray gradient) {
        if (storedGradient == null) {
            storedGradient = gradient.dup('f').assign(0);
            storedDeltas = gradient.dup('f').assign(0);
        }

        // Accumulate gradient
        storedGradient.muli(rho).addi(gradient.mul(gradient).mul(1 - rho));
        
        // Compute update
        INDArray delta = gradient.dup('f').assign(0);
        delta.subi(sqrt(storedDeltas.add(eps)).div(sqrt(storedGradient.add(eps)))).muli(gradient);
        
        // Accumulate update
        storedDeltas.muli(rho).addi(delta.mul(delta).mul(1 - rho));
        
        // Apply update
        array.addi(delta);
        gradient.assign(0);


    }
}
