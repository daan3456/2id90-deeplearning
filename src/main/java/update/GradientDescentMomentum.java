/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package update;

import nl.tue.s2id90.dl.NN.optimizer.update.UpdateFunction;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

/**
 *
 * @author s167501
 */
public class GradientDescentMomentum implements UpdateFunction {
    
    double beta;
    INDArray momentum;
    
    public GradientDescentMomentum(double beta) {
        this.beta = beta;
    }

    @Override
    public void update(INDArray array, boolean isBias, float learningRate, int batchSize, INDArray gradient) {
        if (momentum == null) {
            momentum = gradient.dup('f').assign(0);
        }
        float factor = -(learningRate/batchSize);
        
        momentum.muli(beta);
        Nd4j.getBlasWrapper().level1().axpy(array.length(), (1-beta), gradient, momentum);
        
        Nd4j.getBlasWrapper().level1().axpy( array.length(), factor, momentum, array );
        gradient.assign(0);
    }
    
}
