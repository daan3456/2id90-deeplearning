package update;

import java.util.function.Supplier;
import nl.tue.s2id90.dl.NN.optimizer.update.UpdateFunction;
import org.nd4j.linalg.api.ndarray.INDArray;

/**
 *
 * @author s167501
 */
public class L2Decay implements UpdateFunction {
    
    double decay;
    UpdateFunction f;
    
    public L2Decay(Supplier<UpdateFunction> supplier, double decay) {
        this.decay = decay;
        this.f = supplier.get();
    }

    @Override
    public void update(INDArray array, boolean isBias, float learningRate, int batchSize, INDArray gradient) {
        if (!isBias) {
            double regularization = 1 - learningRate * decay / batchSize;
            array.muli(regularization);
        }
        this.f.update(array, isBias, learningRate, batchSize, gradient);
    }
    
}
