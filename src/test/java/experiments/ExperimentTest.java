/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package experiments;

import nl.tue.s2id90.dl.experiment.Experiment;
import java.io.IOException;
import static java.lang.Math.round;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author s166499
 */
public class ExperimentTest extends Experiment{
    List validations = new ArrayList();
    public void testFunction() throws IOException {
        for (int i=0; i<12; i++) {
            validations.add(new FunctionExperiment().go(10+i*5,10,0.1f));
        }
        System.out.format("final validations for different batch sizes: %s \n", validations);
    }
    public void testZalando() throws IOException {
        for (int i=0; i<15; i++) {
            int batchSize = 16+i*8;
            int epochs = round((float)batchSize*18750/60000);
            validations.add(new ZalandoExperiment().go(batchSize,epochs,0.02f));
        }
        System.out.format("final validations for different batch sizes: %s \n", validations);
    }
    public void testPrimitives() throws IOException {
        for (int i=0; i<3; i++) {
            validations.add(new PrimitivesExperiment().go(10+i*5,10,0.2f));
        }
        System.out.format("final validations for different batch sizes: %s \n", validations);
    }
    
    
    
    public static void main(String[] args) throws IOException {
        new ExperimentTest().testZalando();
    }
}
