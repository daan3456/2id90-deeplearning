/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package update;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.nd4j.linalg.api.complex.IComplexNumber;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.api.rng.distribution.impl.BinomialDistribution;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.conditions.Condition;
import org.nd4j.linalg.indexing.conditions.Conditions;
import org.nd4j.linalg.ops.transforms.Transforms;

/**
 *
 * @author s167501
 */
public class AdaDeltaTest {

    /**
     * Test of update method, of class AdaDelta.
     */
    @Test
    public void testUpdate() {
        System.out.println("update");
        
        float rho = 0.95f;
        float eps = 0.000001f;
        
        INDArray sg = Nd4j.create(new float[] {0.5f, 0.1f, 2f});
        INDArray g = Nd4j.create(new float[] {1, 2, 3});
        INDArray b = Nd4j.create(new float[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        b = Nd4j.rand(b.shape(), new BinomialDistribution(1, 0.5));
        
        System.out.println("bd: " + b);
        
        System.out.println("Div: " + g.div(sg));
        
        System.out.println(sg);
        System.out.println(g);
        
        sg.muli(rho);
        
        System.out.println(sg);
        System.out.println(g);
        
        Nd4j.getBlasWrapper().level1().axpy(g.length(), (1-rho), g.mul(g), sg);
        
        System.out.println(sg);
        System.out.println(g);
        
        System.out.println("power: " + g.mul(g));
        System.out.println("Neg: " + g.neg());
        System.out.println("sqrt: " + Transforms.sqrt(g.muli(g), false));
        System.out.println("g: " + g);
        
        
        INDArray array = Nd4j.create(2);
        array.addi(1);
        System.out.println(array);
        
        AdaDelta instance = new AdaDelta(0.95f, 0.000001f);
        
        for (int i = 0; i < 4; i++) {
        
            INDArray gradient = Nd4j.create(2);
            gradient.addi(6);

            instance.update(array, false, 0f, 0, gradient);

            System.out.println(array);
            
        }
    }
    
}
